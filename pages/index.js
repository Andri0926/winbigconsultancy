import Head from 'next/head'
import ContactSection from '../components/ContactSection'
import WhyChooseUsSection from '../components/WhyChooseUsSection'
import ServicesSection from '../components/ServicesSection'
import FloatingWhatsApp from 'react-floating-whatsapp'
import Footer from '../components/Footer'
import Hero from '../components/Hero'
import FadeInSection from '../components/FadeInSection'
import Navbar from '../components/Navbar'
import InnovativeDigitalSection from '../components/InnovativeDigitalSection'

export default function Home() {

  return (
    <>
      <Navbar/>
      <Hero/>
      <WhyChooseUsSection/>
      <InnovativeDigitalSection/>
      <ServicesSection/>
      <ContactSection/>
      <Footer/>
      <FloatingWhatsApp
        phoneNumber="+27629703851"
        accountName="Winbig Consultancy"
        chatMessage={"Hello there! 🤝\nHow can we help?"}
        avatar={"/profile.png"}
        allowClickAway
        notification
      />
    </>
  )
}