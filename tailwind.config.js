const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  mode: 'jit',
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage:{
        profile:"url(/profile.jpg)",
        coco_oil:"url('/coco_oil.jpg')",
        project_analysis:"url('/Project_Analysis.png')",
        coco_fiber:"url('/coco_fiber.jpg')",
        copra:"url('/copra.jpg')",
      },
    
      fontFamily:{
        roboto:['Roboto', 'sans-serif'],
        forum:['Forum', 'cursive'],
      },
      colors:{
        gray_purpleish: {
          DEFAULT:"#F0ECE3",
          "200": "#968C83",
          "300": "#968C83",
          "400":"#C3B091",
          "500": "#8E806A"
        },
        custom_gray: {
          DEFAULT: "#EAEAEA",
          "200": "#F5FBFA",
          "300": '#212324',
          
        },
        brown:{
          DEFAULT:"#FFE1BD"
        },
        dark_blue:{
          DEFAULT:"#102C3A",
          "200":"#2FB6B0",
        }
      }
    },
  },
  plugins: [
    (function({addUtilities}){
      const utilities = {
        ".bg-hero":{
          "background-image": "url(/wb_hero.png)",
          "background-size": "cover",
          "background-position":"left",
          "background-repeat":"no-repeat"
        },
        ".shadow-custom":{
          "box-shadow": "0px 15px 30px rgba(0,0,0,0.1)"
        }
      };

      addUtilities(utilities);
    })
  ],
}