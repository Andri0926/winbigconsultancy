import classnames from "classnames"

export default function FooterNavItem({children, href}){
    
    const onClickAbout=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById(href);
        elementToView.scrollIntoView(); 
    }
    
    return(
        <li>
            <a 
                onClick={onClickAbout.bind(this)}
                className="text-gray-100 hover:text-orange-600 transition duration-200 ease-in-out text-sm cursor-pointer font-roboto"
            >
                {children}
            </a>
        </li>
    );
}