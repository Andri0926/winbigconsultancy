export default function Hero(){
    const navigate=(e, id) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById(id);
        elementToView.scrollIntoView(); 
    }
    return(
        <section className='container mx-auto' id='home'>
            <div className="h-[700px] xl:h-[600px] min-w-full grid place-content-center bg-hero">
                
                <div className="px-10 pt-10 text-center lg:text-left lg:ml-40 w-full lg:pt-12 flex flex-col">
                    <h1 className="text-3xl lg:text-6xl text-white font-forum tracking-wide lg:w-5/12 px-5 mt-10">Grow Your Business And Career With Us.</h1>
                    
                    <p className="md:text-sm text-white pt-6 md:tracking-wide text-justify lg:w-5/12 px-5 font-roboto font-light">
                        Winbig is a registered reputable company authorised to provide consultancy services and innovative solutions to businesses, academics, professional bodies and individuals in the following areas: Digital services and Products, Investment and Project consultancy, Industrial Research, Business Financing, Business Plan Development, Thesis and Dissertation Research Support, Skills Training and Development, Proof Reading Services.
                    </p>
                    <div className="lg:w-5/12 font-roboto">
                        <div className="text-center ">
                            <button className="rounded-sm bg-orange-600 hover:bg-orange-700 transition duration-200 ease-in-out text-white py-2 px-8 mt-10" href="#" onClick={(e=>navigate(e, 'services'))}>
                                SERVICES
                            </button>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            
        </section>
    )
}