import DigitalServices from "./icon/DigitalServices";
import Investment from "./icon/Investment";
import Industrial from "./icon/Industrial";
import BusinessFinancing from "./icon/BusinessFinancing";
import BusinessPlanDev from "./icon/BusinessPlanDev";
import SkillTrainingDev from "./icon/SkillTrainingDev";
import ThesisDissertation from "./icon/ThesisDissertation";
import ProofReading from "./icon/ProofReading";


export default function WhyChooseUsSection(){

    const onClickToDigitalSolutions=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("digitalsolutionsandproducts");
        elementToView.scrollIntoView(); 
    }
    const onClickToinvestmentprojectanalysis=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("investmentprojectanalysis");
        elementToView.scrollIntoView(); 
    }
    const onClickToindustrialresearch=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("industrialresearch");
        elementToView.scrollIntoView(); 
    }
    
    const onClickTobusinessfinancing=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("businessfinancing");
        elementToView.scrollIntoView(); 
    }
    
    const onClickTodissertationthesis=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("dissertationthesis");
        elementToView.scrollIntoView(); 
    }

    const onClickToskilldev=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("skilldev");
        elementToView.scrollIntoView(); 
    }

    const onClickTobusinessplandev=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("businessplandev");
        elementToView.scrollIntoView(); 
    }
    //proofreadingpublications
    const onClickToproofreadingpublications=(e) =>{
        e && e.preventDefault(); // to avoid the link from redirecting
        const elementToView = document.getElementById("proofreadingpublications");
        elementToView.scrollIntoView(); 
    }
  
    return(
        <section className='py-20' id='about'>
            <div className='container mx-auto xl:px-24 px-4'>
                <div className="flex flex-wrap">
                    <div className="md:w-6/12 w-full space-y-8 px-4">
                        <div className="mx-8">
                            <h2 className="text-5xl font-forum tracking-wide">Why Choose Us</h2>
                            <p className="font-roboto font-light tracking-widest pt-4 text-justify text-sm">
                                At Winbig consultancy, we make your dream and aspiration become a reality and our testimony because we have a team of well-blended experienced and competent professionals and specialists who understand your needs, see possibilities in challenges, strive for excellence and high customer satisfaction with integrity, transparency and trust. We excel in providing the following services to our valued customers:
                            </p>
                            <div className="text-orange-600 pt-8 pb-10 text-sm font-roboto font-light space-y-2 flex flex-col">
                                <div className="flex space-x-4">
                                    <DigitalServices/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickToDigitalSolutions.bind(this)}>Digital Services and Products</a>
                                </div>
                                <div className="flex space-x-4">
                                    <Investment/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickToinvestmentprojectanalysis.bind(this)}>Investment and Project Consultancy</a>
                                </div>
                                
                                <div className="flex space-x-4">
                                    <Industrial/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickToindustrialresearch.bind(this)}>Industrial Research</a>
                                </div>
                                
                                <div className="flex space-x-4">
                                    <BusinessFinancing/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickTobusinessfinancing.bind(this)}>Business Financing</a>
                                </div>
                                
                                <div className="flex space-x-4">
                                    <ThesisDissertation/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickTodissertationthesis.bind(this)}>Thesis and Dissertation Research Support</a>
                                </div>

                                <div className="flex space-x-4">
                                    <SkillTrainingDev/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickToskilldev.bind(this)}>Skill Training and Development</a>
                                </div>

                                <div className="flex space-x-4">
                                    <ProofReading/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickToproofreadingpublications.bind(this)}>Proof Reading Services</a>
                                </div>

                                <div className="flex space-x-4">
                                    <BusinessPlanDev/>
                                    <a className="no-underline hover:underline cursor-pointer" onClick={onClickTobusinessplandev.bind(this)}>Business Plan Development</a>
                                </div>
                                
                                
                                

                            </div>
                        </div>
                        
                    </div>

                    <div className="w-full md:w-6/12 md:pl-10">
                        <img src="/whychooseus.png" alt="why choose us" />
                    </div>
                   
                </div>
            </div>
        </section>
    )
}