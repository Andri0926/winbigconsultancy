import { useState, useEffect } from "react";
import Nav from "./Nav";
import classnames from "classnames";
import Logo from "./icon/logo";
import Menu from "./icon/Menu";

export default function Navbar() {
    const [offcanvas, setOffcanvas] = useState(false);
    const [offset, setOffset] = useState(0);
    const [heightPercentage, setHeightPercentage] = useState("0%");
    
    const scale = (number, [inMin, inMax], [outMin, outMax]) => {
        // if you need an integer value use Math.floor or Math.ceil here
        return (number - inMin) / (inMax - inMin) * (outMax - outMin) + outMin;
    }



    useEffect(() => {
        const onScroll = () => setOffset(window.pageYOffset);

        var limit = Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight) - window.innerHeight;
        
        setHeightPercentage(Math.round(scale(offset, [0, limit], [0,100])) + "" +"%")
        
        // clean up code
        window.removeEventListener('scroll', onScroll);
        window.addEventListener('scroll', onScroll, { passive: true });
        return () => window.removeEventListener('scroll', onScroll);
    }, [offset]);

    return (
        <div className="fixed top-0 z-50 w-full">
            <div className="container mx-auto bg-neutral-700 bg-opacity-60 ">
                
                <div className={classnames("flex items-center justify-center px-8 transition-all ease-in duration-300 h-24", offset>=60? 'h-14 shadow-lg' : 'h-24')}>

                    <div className="w-4/12">
                        <div className={classnames("flex", offset>=60? 'h-[150px] w-[150px] lg:h-[240px] lg:w-[240px]' : 'h-[200px] w-[200px] lg:h-[350px] lg:w-[350px]')}>
                            <Logo/>
                        </div>
                    </div>

                    <button className="w-8/12 md:hidden text-right" onClick={() => setOffcanvas(true)}>
                        <div className="inline-block">
                            <Menu/>
                        </div>
                    </button>

                    <div className="w-8/12 hidden md:block">
                        <Nav dir="horizontal" setOffcanvas={setOffcanvas} />
                    </div>
                </div>
                <div className="w-full">
                    <div className="bg-orange-500 h-1 transition-all ease-in duration-100" style={{width: heightPercentage}}></div>
                </div>

                <div className={classnames("fixed bg-white z-10 top-0 h-full w-full p-10 md:hidden transition-all", offcanvas ? "right-0" : "-right-full")}>
                    <img src="/x.svg" className="absolute top-8 right-8 w-8" onClick={() => setOffcanvas(false)} alt="consultancy"/>
                    <Nav
                        dir="vertical"
                        setOffcanvas={setOffcanvas}
                    />
                </div>
                
            </div>
            
        </div>
    );
}