

export default function InnovativeDigitalSection(){
    return(
        <section className="py-0" id='digitalsolutionsandproducts'>
            <div className="container mx-auto xl:px-24 bg-neutral-50">
                
                <h1 className="text-4xl text-center font-forum pt-10">Innovative Digital Solutions:</h1>
                <h2 className="text-xl text-center text-orange-600 pb-4 font-roboto font-light">We provide the following digital services and products</h2>

                <div className="bg-white lg:mx-52">
                    <div className="flex justify-center">
                        <img src="/innovative_digital.png" alt="Innovative Digital Solutions"/>
                    </div>
                    <div className="pt-10 pb-10 space-y-10 px-10">
                        <div>
                            <h2 className="text-4xl font-roboto font-light">Innovative Digital Services:</h2>
                            <p className="text-justify font-roboto font-light">
                                We provide cutting edge innovative digital solutions and marketing services such as local optimization, search engine optimization and google ranking, website creation and website design, video creation and video marketing, social media marketing and management, advertising and consulting among many others.
                            </p>
                        </div>
                        <div>
                            <h2 className="text-4xl font-roboto font-light">Winbig Ready-Made Stores:</h2>
                            <p className="text-justify font-roboto font-light">
                                At Winbig, you become a lucrative business owner in just less than 24hrs by choosing  from over 40 different web-based Winbig  stores stocked with over 230 high winning products readily available for delivering in just less than 24hrs. This is accompanied with 24hrs/7 services and link to direct target markets. You also have an option to be our partner for Winbig products and services. To see the different stores available for delivery, please click on <a href="https://www.winbigstores.co.za/" target="_blank" rel="noreferrer" className="text-orange-600 no-underline hover:underline cursor-pointer">https://www.winbigstores.co.za/</a>
                            </p>
                        </div>
                        <div>
                            <h2 className="text-4xl font-roboto font-light">Digital Products:</h2>
                            <p className="text-justify font-roboto font-light">
                                We supply advanced digital infrastructure, cloud services, software, and other digital products to support services for our clients, making transition to digital services easy and more convenient. To see the available digital products and infrastructure, please click <a href="https://www.winbigtech.com/" target="_blank" rel="noreferrer" className="text-orange-600 no-underline hover:underline cursor-pointer">https://www.winbigtech.com/</a>
                            </p>
                        </div>
                        <div>
                            <h2 className="text-4xl font-roboto font-light">Digital Training:</h2>
                            <p className="text-justify font-roboto font-light">
                                At Winbig, we don&apos;t only provide digital services and products, but we also provide digital literacy training to our valued clients for sustainable business operation. This we do by training our clients to use the platforms and digital products efficiently to meet their business targets. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}