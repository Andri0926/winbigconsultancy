import React from "react";

function Mail() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="25"
      height="25"
      fill="none"
      viewBox="0 0 25 25"
    >
      <path
        fill="#fff"
        d="M21.321 5H3.68A.68.68 0 003 5.682v13.636a.68.68 0 00.679.682H21.32a.68.68 0 00.679-.682V5.682A.68.68 0 0021.321 5zm-1.713 2.32l-6.69 5.231a.454.454 0 01-.562 0l-6.693-5.23a.153.153 0 01.094-.276h13.758a.152.152 0 01.146.198.153.153 0 01-.053.077z"
      ></path>
    </svg>
  );
}

export default Mail;
