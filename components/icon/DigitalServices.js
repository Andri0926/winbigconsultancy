import React from "react";

function DigitalServices() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#143243"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M5 15V8M5.25 22a3.25 3.25 0 100-6.5 3.25 3.25 0 000 6.5zM5 8a3 3 0 100-6 3 3 0 000 6zM19 8a3 3 0 100-6 3 3 0 000 6zM5.13 15a4.058 4.058 0 013.94-3.04l3.43.01c2.62.01 4.85-1.67 5.67-4.01"
      ></path>
    </svg>
  );
}

export default DigitalServices;
