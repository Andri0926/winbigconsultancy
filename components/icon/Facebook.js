import React from "react";

function Facebook() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="27"
      height="27"
      fill="none"
      viewBox="0 0 27 27"
    >
      <path
        fill="#fff"
        d="M25.92 0H1.08C.483 0 0 .483 0 1.08v24.84C0 26.517.483 27 1.08 27h24.84c.597 0 1.08-.483 1.08-1.08V1.08C27 .483 26.517 0 25.92 0zm-3.119 7.88h-2.156c-1.691 0-2.018.804-2.018 1.985v2.602h4.036l-.526 4.074h-3.51V27h-4.209V16.544h-3.52v-4.077h3.52V9.463c0-3.486 2.13-5.386 5.241-5.386 1.492 0 2.771.111 3.146.162v3.642h-.003z"
      ></path>
    </svg>
  );
}

export default Facebook;
