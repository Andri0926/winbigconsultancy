import React from "react";

function CostBenefit() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="31"
      fill="none"
      viewBox="0 0 30 31"
    >
      <g
        stroke="#EA5226"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        filter="url(#filter0_d_281_10)"
      >
        <path d="M5 2v17c0 1.66 1.34 3 3 3h17"></path>
        <path d="M8 17l4.59-5.36c.76-.88 2.11-.94 2.93-.11l.95.95c.82.82 2.17.77 2.93-.11L24 7"></path>
      </g>
      <defs>
        <filter
          id="filter0_d_281_10"
          width="32"
          height="32"
          x="-1"
          y="0"
          colorInterpolationFilters="sRGB"
          filterUnits="userSpaceOnUse"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
          <feColorMatrix
            in="SourceAlpha"
            result="hardAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          ></feColorMatrix>
          <feOffset dy="4"></feOffset>
          <feGaussianBlur stdDeviation="2"></feGaussianBlur>
          <feComposite in2="hardAlpha" operator="out"></feComposite>
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
          <feBlend
            in2="BackgroundImageFix"
            result="effect1_dropShadow_281_10"
          ></feBlend>
          <feBlend
            in="SourceGraphic"
            in2="effect1_dropShadow_281_10"
            result="shape"
          ></feBlend>
        </filter>
      </defs>
    </svg>
  );
}

export default CostBenefit;
