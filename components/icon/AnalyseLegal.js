import React from "react";

function AnalyseLegal() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#EA5226"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        d="M2 22h20M12 2c1.6.64 3.4.64 5 0v3c-1.6.64-3.4.64-5 0V2zM12 5v3M17 8H7c-2 0-3 1-3 3v11h16V11c0-2-1-3-3-3zM4.58 12h14.84"
      ></path>
      <path
        stroke="#EA5226"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        d="M7.99 12v10M11.99 12v10M15.99 12v10"
      ></path>
    </svg>
  );
}

export default AnalyseLegal;
