

function RequiredFieldAlert() {
    return (

        <div className="pl-4 pr-10 leading-normal text-white bg-orange-600 rounded-lg" role="alert">
            <span className="block sm:inline text-white font-roboto"> This Field is required.</span>
        </div> 
    )
}

export default RequiredFieldAlert
