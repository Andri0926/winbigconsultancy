export default function Footer(){
    return(
        <footer className="text-sm tracking-wider py-6 text-center border-t-2 border-gray-500 bg-custom_gray-300">
            <p className="text-gray-500 font-roboto font-light">@ 2022 SEC! Development. All Rights Reserved 2022.</p>
        </footer>
    )
}