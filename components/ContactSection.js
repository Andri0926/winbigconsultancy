import emailjs from "emailjs-com";
import apiKeys from "../pages/api/apikeys";
import { useForm } from 'react-hook-form';
import RequiredFieldAlert from "./RequiredFieldAlert";
import { ToastContainer, toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Logo from "./icon/logo";
import FooterNavItem from "./FooterNavItem";
import Mail from "./icon/Mail";
import Facebook from "./icon/Facebook";
import Linkedin from "./icon/Linkedin";
import Instagram from "./icon/Instagram";
import Whatsapp from "./icon/Whatsapp";
import Smartphone from "./icon/Smartphone";
export default function ContactSection(){
    
    const { register, handleSubmit, formState: { errors, isSubmitting, isSubmitSuccessful }, reset } = useForm();

    const kontakForm= (data, e)=>{
        e.preventDefault()
        
        emailjs.sendForm(apiKeys.SERVICE_ID, apiKeys.TEMPLATE_ID, e.target, apiKeys.USER_ID).then(
            result => {
                notifySuccess('Message Sent, I\'ll get back to you shortly')
                
            },
            error => {
                notifyError('An error occured, Plese try again')
        })
        e.target.reset();
        reset();
    }

    const notifySuccess = (text) => toast.success(text, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        toastId: "notifySuccess"
    });

    const notifyError = (text) => toast.error(text, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        toastId: "notifyError"
    });
    

    return(
        <section className="container mx-auto" id='contact'>
            <div className="flex bg-custom_gray-300 flex-wrap">
                <div className="lg:w-5/12 w-full lg:py-16 lg:px-16 p-10">
                    <div className="">
                        <Logo/>
                    </div>
                    <p className="text-gray-300 mx-10 tracking-widest leading-relaxed text-sm text-justify font-roboto font-light">
                        Winbig is a registered reputable consultancy company authorized to assist businesses, academics and other professional advisory needs for all individuals. Our team of experts have extensive consultancy knowledge to assist businesses with a variety of services that will give them an edge over their competitors, advance in their careers and operate with sustainability and excellence. Our expertise cuts across industries, sectors and fields of study. Years of research knowledge, communication skills and writing skills set us apart from the rest.
                    </p>
                    
                </div>

                <div className="lg:w-3/12 w-full space-y-6 lg:py-16 grid lg:block justify-items-center mt-10 pb-10 lg:pt-24 lg:pb-0 font-roboto">
                    <div>
                        <h2 className="text-4xl text-white ">Contact Us</h2>    
                        <p className="text-2xl w-5/12 border-b-4 border-orange-600 pt-2 font-roboto"></p>
                    </div>
                    
                    <div className="flex space-x-6">
                        <div className="">
                            <Mail/>
                        </div>
                        <a className="text-white tracking-wider no-underline hover:underline cursor-pointer" href="mailto:info@winbigconsultancy.co.za">info@winbigconsultancy.co.za</a>
                    </div>

                    <div className="flex space-x-6">
                        <div className="">
                            <Smartphone/>
                        </div>
                        <div className="text-white tracking-wider">
                            <a className="no-underline hover:underline cursor-pointer" href="tel:+27415810489">+27415810489</a>
                            <span className=""> / </span>
                            <a className="no-underline hover:underline cursor-pointer" href="tel:+27629703851">+27629703851</a>
                        </div>
                    </div>

                    <div className="flex space-x-6">
                        <div className="">
                            <Whatsapp/>
                        </div>
                        <a className="text-white tracking-wider no-underline hover:underline cursor-pointer" href="https://api.whatsapp.com/send/?phone=+27629703851&text=Hi Winbig Consultancy" target="_blank" rel="noreferrer">+27629703851</a>
                    </div>

                    <div className="flex space-x-6">
                        <a href="https://www.facebook.com/profile.php?id=100080593065660" target="_blank" rel="noopener noreferrer">
                            <Facebook/>
                        </a>
                        <a href="https://www.linkedin.com/in/winbig-consultancy-064ab9238" target="_blank" rel="noopener noreferrer">
                            <Linkedin/>
                        </a>
                        <a href="https://www.instagram.com/winbigconsultancy" target="_blank" rel="noopener noreferrer">
                            <Instagram/>
                        </a>
                    </div>

                    <div className="">
                        <ul className="flex space-x-4">
                            <FooterNavItem href='home'>Home</FooterNavItem>
                            <FooterNavItem href='about'>About</FooterNavItem>
                            <FooterNavItem href='digitalsolutionsandproducts'>Digital Solutions</FooterNavItem>
                            <FooterNavItem href='services'>Other Services</FooterNavItem>
                        </ul>
                    </div>
                    
                    
                </div>

                <div className="lg:w-4/12 w-full">
                    <form className="lg:py-16 p-10 lg:px-20" onSubmit = {handleSubmit(kontakForm)}>
                        
                        <div className="mb-6">
                            <label htmlFor="full_name" className="block text-lg text-white font-roboto">Full Name *</label>
                            <input type="text" name="full_name" id="full_name" {...register("full_name", {required: "Required",})} className="py-1 px-6 w-full rounded"/>
                            {
                                errors?.full_name &&
                                <RequiredFieldAlert/>                            
                            }
                        </div>

                        <div className="mb-6">
                            <label htmlFor="contact_number" className="block text-lg text-white font-roboto">Contact Number *</label>
                            <input 
                                type="text" 
                                name="contact_number" 
                                id="contact_number" 
                                {...register("contact_number", {
                                    pattern: /^\+?([ -]?\d+)+|\(\d+\)([ -]\d+)$/,
                                    required: "Required",
                                })}
                                className="py-1 px-6 w-full rounded"
                            />
                            {
                                errors?.contact_number &&
                                <div className="pl-4 pr-10 leading-normal text-white bg-orange-600 rounded-lg" role="alert">

                                    { errors.contact_number?.type === 'required' && 
                                        <div>
                                            <span className="block sm:inline text-white font-roboto"> This Field is required.</span>
                                        </div>
                                    }

                                    { errors.contact_number?.type === 'pattern' && 
                                        <div>
                                            <span className="block sm:inline text-white font-roboto"> Your Phone Number is Invalid.</span>
                                        </div>
                                    }

                                </div>                           
                            }
                        </div>
                        <div className="mb-6">
                            <label htmlFor="email" className="block text-lg text-white font-roboto">Email *</label>
                            <input type="text" name="email" id="email" 
                                className="py-1 px-6 w-full rounded"
                                {...register("email", {
                                    required: "Required",
                                    pattern: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                })}
                            />
                            {
                                errors?.email &&
                                <div className="pl-4 pr-10 leading-normal text-white bg-orange-600 rounded-lg font-roboto" role="alert">
                                    { errors.email?.type === 'required' && 
                                        <div>
                                            <span className="block sm:inline text-white font-roboto">This Field is required.</span>
                                        </div>
                                    }

                                    { errors.email?.type === 'pattern' && 
                                        <div>
                                            <span className="block sm:inline text-white font-roboto"> Your Email Address is Invalid.</span>
                                        </div>
                                    }
                                </div>
                            }
                        </div>
                        <div className="mb-6">
                            <label htmlFor="message" className="block text-lg text-white font-roboto">Message *</label>
                            <textarea type="textarea" name="message" id="message" {...register("message", {required: "Required",})} className="px-6 w-full h-36 rounded-lg"/>
                            {
                                errors?.message &&
                                <RequiredFieldAlert/>                            
                            }
                        </div>
                        
                        <div className="cursor-pointer text-left">
                            <button 
                                type="submit" 
                                className="bg-orange-600 hover:bg-opacity-50 text-white transition py-2 px-6 rounded text-lg inline-block font-roboto"
                                disabled={isSubmitting}
                            >
                                { isSubmitting ?
                                    <div className=" flex justify-center items-center">
                                        <div className="animate-spin rounded-full h-8 w-8 border-t-2 border-b-2 border-white"></div>
                                    </div>
                                    :
                                    <p className=''>Submit</p>
                                }
                            </button>
                        </div>
                        <ToastContainer
                            position="top-center"
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss={false}
                            draggable={false}
                            pauseOnHover={false}
                            transition={Zoom}
                        />
                    </form>
                </div>
            </div>
            
        </section>
    )
}