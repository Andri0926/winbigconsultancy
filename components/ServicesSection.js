import CostBenefit from "./icon/CostBenefit";
import RankingClient from "./icon/RankingClient";
import EstimateBenefit from "./icon/EstimateBenefit";
import EstimateMarket from "./icon/EstimateMarket"
import AnalyseTechnical from "./icon/AnalyseTechnical";
import AnalyseOperational from "./icon/AnalyseOperational";
import AnalyseLegal from "./icon/AnalyseLegal";
import AnalyseScheduling from "./icon/AnalyseScheduling";
import ConductingIndustry from "./icon/ConductingIndustry";
import LocateCompetitors from "./icon/LocateCompetitors";
import Porters from "./icon/Porters";
import SWOT from "./icon/SWOT";
import PEST from "./icon/PEST";
import DevelopPublication from "./icon/DevelopPublication";
import ManuscriptRevision from "./icon/ManuscriptRevision";
import ScientificEditing from "./icon/ScientificEditing";
import TrainingProgrammes from "./icon/TrainingProgrammes";
import SoftSkill from "./icon/SoftSkill";
import YouthTraining from "./icon/YouthTraining";
import Moderate from "./icon/Moderate";
import Other from "./icon/Other";
import BusinessPlans from "./icon/BusinessPlans";
import Guarantee from "./icon/Guarantee";

export default function ProductsSection(){
    return(
        <section className="py-0" id='services'>
            <div className="container mx-auto xl:px-24 bg-neutral-100">
                <div className="flex flex-wrap pt-4 pb-4">
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='investmentprojectanalysis'>Investment &amp; Project Consultancy</h2>
                        <img src="/investment_project.png" alt="Investment and Project Consultancy"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            Our team is a host of experienced Project analysis consultants that will go above and beyond to create a “no lose” situation for your desired project.  We simplify this complex business decision by performing a project analysis to measure the benefits of your pursued project.  Our team has great knowledge in measuring different financial metrics  to assist clients compare different projects based on their net benefits.
                        </p>

                        <p className="font-roboto pb-2 font-light">This Service includes:</p>

                        <div className="flex flex-wrap font-roboto font-light">
                            <div className="md:w-6/12 space-y-2">
                                <div className="flex space-x-4">
                                    <div className="h-8 w-8">
                                        <CostBenefit/>
                                    </div>
                                    <p>Cost-Benefit Analysis</p>
                                </div>
                                <div className="flex space-x-6">
                                    <div className="h-8 w-8">
                                        <RankingClient/>
                                    </div>
                                    <p>Ranking clients&apos; project according to their merits</p>
                                </div>
                                <div className="flex space-x-4">
                                    <div className="h-8 w-8">
                                        <EstimateBenefit/>
                                    </div>
                                    <p>Estimate the net benefits of a project</p>
                                </div>
                                <div className="flex space-x-6">
                                    <div className="h-8 w-8">
                                        <EstimateMarket/>
                                    </div>
                                    <p>Estimate the market and financial feasibility of projects</p>
                                </div>
                                
                            </div>
                            <div className="md:w-6/12 space-y-2">
                                <div className="flex space-x-5">
                                    <div className="h-8 w-8">
                                        <AnalyseTechnical/>
                                    </div>
                                    <p>Analyse the technical feasibility of projects</p>
                                </div>
                                <div className="flex space-x-4">
                                    <div className="h-8 w-8">
                                        <AnalyseOperational/>
                                    </div>
                                    <p>Analyse operational feasibility</p>
                                </div>
                                <div className="flex space-x-4">
                                    <div className="h-8 w-8">
                                        <AnalyseLegal/>
                                    </div>
                                    <p>Analyse the legal feasibility of projects</p>
                                </div>
                                <div className="flex space-x-4">
                                    <div className="h-8 w-8">
                                        <AnalyseScheduling/>
                                    </div>
                                    <p>Analyse scheduling feasibility</p>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                    
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='industrialresearch'>Industrial Research</h2>
                        <img src="/industrial_research.png" alt="Industrial Research"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            Conducting an industry and market analysis is one of the important steps when writing a business plan, making strategic decisions, and pursuing competitive advantage. Our team has extensive knowledge in conducting industry analysis to assist you, understand the business models of your competitors and other key players in your industry. Our industry analysis includes information on whether your industry is growing or shrinking, industry forecasts and studying industry trends that might have an impact on your business. This service includes the following:
                        </p>
                        <div className="font-roboto space-y-2 font-light">
                            <div className="flex space-x-4 ">
                                <div className="h-8 w-8">
                                    <ConductingIndustry/>
                                </div>
                                <p>Conducting industry background</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <LocateCompetitors/>
                                </div>
                                <p>Locate competitors/industry leaders</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <Porters/>
                                </div>
                                <p>Porters Five Forces</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <PEST/>
                                </div>
                                <p>PEST Analysis</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <SWOT/>
                                </div>
                                <p>SWOT Analysis</p>
                            </div>
                        </div>
                        
                    </div>
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='businessfinancing'>Business Financing</h2>
                        <img src="/business_financing.png" alt="Business Financing"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            Funding your business is one of the fundamental steps to start or expand capital for your business. Take advantage of our skills to communicate your business ideas with potential stakeholders and help secure funding opportunities for your business. Irrespective of sector, our team has a deep understanding of how markets operate and will offer extraordinary solutions to improve the financial health of your business. We offer sound, informed and detailed financial forecasting to help you achieve your short-term and long-term financial goals.
                        </p>
                    </div>
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4 font-roboto">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed"  id='dissertationthesis'>Dissertation &amp; Thesis Research Support</h2>
                        <img src="/dissertation_thesis.png" alt="Dissertation and Thesis Research Support"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            In the very competitive academic publishing world, our step-by-step support and guide through your manuscript will guarantee that the quality of your article surpasses those of your field-peers. Our research experts will measure the impact of your article and assist you increase your chances of publishing your article with high impact journals.
                        </p>
                        <p className="pb-2 font-light">Our services includes:</p>
                        <div className="font-roboto space-y-2 font-light">
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <DevelopPublication/>
                                </div>
                                <p>Develop a publication plan</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <ManuscriptRevision/>
                                </div>
                                <p>Manuscript revision</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <ScientificEditing/>
                                </div>
                                <p>Scientific editing</p>
                            </div>
                        </div>
                    </div>
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='skilldev'>Skill Development &amp; Training</h2>
                        <img src="/skill_dev_training.png" alt="Skill Development and Training"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            We have a team of qualified professionals and specialists who provide the following services:
                        </p>
                        <div className="font-roboto space-y-2 font-light">
                            <div className="flex space-x-5">
                                <div className="h-8 w-8">
                                    <TrainingProgrammes/>
                                </div>
                                <p>Develop Training Programmes for employees and Partners in upskilling, cross-skilling and reskilling</p>
                            </div>
                            <div className="flex space-x-6">
                                <div className="h-8 w-8">
                                    <SoftSkill/>
                                </div>
                                <p>It identifies the gaps and develops the skills which enable the person to achieve their goals soft skill training</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <YouthTraining/>
                                </div>
                                <p>Youth training programme</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <Moderate/>
                                </div>
                                <p>Moderate Training programme</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <Other/>
                                </div>
                                <p>Other</p>
                            </div>

                        </div>
                    </div>
                    <div className="md:w-6/12 w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='proofreadingpublications'>Proofreading &amp; Publications</h2>
                        <img src="/proofreading_publication.png" alt="Proofreading and Publications"/>
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            Our team of language editing experts aims for quality conscious writing for all levels of authors. We will assist you with improving grammar, proofreading, sentence construction, improving tone, style and register for your piece of writing to convey its intended message. We offer our services to a variety of clients; Academic essays and assignments, English as a second language speakers, corporate financial reports and business related documents, personal letters, cover letters for job applications and emails, ghostwriting, indexing, proofreading, magazine article writing, and copywriting among others.We also provide publication assistance for academics and authors.
                        </p>
                    </div>
                    <div className="w-full px-4 pt-4 pb-4">
                        <h2 className="text-3xl font-forum tracking-wide leading-relaxed" id='businessplandev'>Business Plan Development Consultancy</h2>
                        <img src="/business_plan_dev.png" alt="Business Plan Development Consultancy"/>
                        
                        <p className="text-justify pt-4 pb-4 font-roboto font-light">
                            Looking to start a business or expand the services of an existing business? We are experts in writing business plans and we offer strategic tools for entrepreneurs to make their business ideas a success. Our team understands the essence of planning and will assist clients with setting relevant short-term and long-term objectives for their business. Our team has the skills to help you develop a well-documented and excellent business plan that will speak for itself. We will help clients build a valuable business plan by eliminating potential pitfalls/weaknesses. We will customise your business plan to best suit your industry. Why choose us?
                        </p>
                        <div className="space-y-2 font-light font-roboto">
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <BusinessPlans/>
                                </div>
                                <p>Our business plans are investor and funding friendly</p>
                            </div>
                            <div className="flex space-x-4">
                                <div className="h-8 w-8">
                                    <Guarantee/>
                                </div>
                                <p>We guarantee first time success</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

    )
}