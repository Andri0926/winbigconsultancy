import NavItem from "./NavItem";
import classNames from "classnames";
import { useState } from "react";

export default function Nav({dir, setOffcanvas}){
    const dirs ={
        horizontal: "space-x-6 lg:space-x-20",
        vertical: "flex-col space-y-6"
    }

    const pickedDir = dirs[dir]
    
    return(
        <ul className={classNames('flex', pickedDir)} onClick={() => setOffcanvas(false)}>

            <NavItem href='home'>Home</NavItem>
            <NavItem href='about'>About</NavItem>
            <NavItem href='digitalsolutionsandproducts'>Digital Solutions & Products</NavItem>
            <NavItem href='services'>Other Services</NavItem>
            <NavItem href='contact'>Contact</NavItem>
        </ul>
    )
}